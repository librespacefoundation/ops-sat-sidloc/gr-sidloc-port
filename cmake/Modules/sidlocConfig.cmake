INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_SIDLOC sidloc)

FIND_PATH(
    SIDLOC_INCLUDE_DIRS
    NAMES sidloc/api.h
    HINTS $ENV{SIDLOC_DIR}/include
        ${PC_SIDLOC_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    SIDLOC_LIBRARIES
    NAMES gnuradio-sidloc
    HINTS $ENV{SIDLOC_DIR}/lib
        ${PC_SIDLOC_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SIDLOC DEFAULT_MSG SIDLOC_LIBRARIES SIDLOC_INCLUDE_DIRS)
MARK_AS_ADVANCED(SIDLOC_LIBRARIES SIDLOC_INCLUDE_DIRS)

