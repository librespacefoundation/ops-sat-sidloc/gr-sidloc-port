#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <cstring>
#include <sstream>
#include <string>

#include "SEPP_SDR_API.h"

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cerr <<argc <<  " Usage: " << argv[0] << " <rx-sampling-rate>" << " <rx-freq-ghz>" << " <path-to-binary>" << std::endl;
    return EXIT_FAILURE;
  }

  try {
    SEPP_SDR_API* sepp = new SEPP_SDR_API();
    sepp->Init_RFFE_Registers();
    sepp->Set_RX_Carrier_Frequency_in_GHz(atof(argv[2]));
    sepp->Set_RX_Sampling_Frequency(eSDR_RFFE_RX_SAMPLING_FREQ(atoi(argv[1])));
    sepp->Enable_Receiver();
  }
  catch (SEPP_SDR_API_Exception e) {
    std::runtime_error(e.what());
  }

  std::stringstream s;
  s << "toGround/log_" << std::time(0);

  const std::string binary_path(argv[3]);
  const std::string logfile(s.str());
  const std::string command(binary_path + " "  + logfile);
//+ "toGround/" + output_filename + " "
  if (std::FILE* file = std::fopen(binary_path.c_str(), "r")) {
    std::fclose(file);
    std::cout << logfile << std::endl;
    const int return_code = std::system(command.c_str());
    if (return_code != 0) {
      std::cerr << "Execution of binary failed with return code " << return_code << std::endl;
      return EXIT_FAILURE;
    }
  } else {
    std::cerr << "Binary not found: " << binary_path << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
