/********************
GNU Radio C++ Flow Graph Source File

Title: Not titled yet
Author: nkaramolegos
GNU Radio version: 3.7.13.5
********************/

#include "opssat_rx.hpp"

using namespace gr;


opsatRX::opsatRX(const char *filename)
{


  this->tb = gr::make_top_block("Not titled yet");

// Blocks:

  this->sidloc_ccsds_non_coherent_return_0 =
    sidloc::ccsds_non_coherent_return::make_shared(18);
  this->coherent_window = 1;
  this->dwells = 10;
  this->threshold = 0.5;
  this->max_offset = 1.9e3;
  this->sidloc_coarse_freq_estimation_0 = sidloc::coarse_freq_estimation::make(
      samp_rate, chip_rate, this->sidloc_ccsds_non_coherent_return_0,
      this->coherent_window, this->dwells, this->threshold, this->max_offset,
      filename);
  this->opssat_dev_source_0 = sidloc::opssat_dev_source::make("/dev/uio0", "/dev/uio1", " ");
  this->low_pass_filter_0_0 = filter::fir_filter_ccf::make(
        30,
        gr::filter::firdes::low_pass(
          1,
          1.5e6,
          29e3,
          5e3,
          filter::firdes::win_type::WIN_HAMMING,
          6.76));

  this->analog_agc2_xx_0 = analog::agc2_cc::make((1e-1), (1e-2), (1.41421 / 2.0),
                           1.0);
  this->analog_agc2_xx_0->set_max_gain(65536);


// Connections:
  this->tb->hier_block2::connect(this->opssat_dev_source_0, 0,
                                this->analog_agc2_xx_0, 0);
  this->tb->hier_block2::connect(this->analog_agc2_xx_0, 0,
                                this->low_pass_filter_0_0, 0);
  this->tb->hier_block2::connect(this->low_pass_filter_0_0, 0,
                                 this->sidloc_coarse_freq_estimation_0, 0);
}

opsatRX::~opsatRX()
{
}

// Callbacks:
double opsatRX::get_samp_rate() const
{
  return this->samp_rate;
}

void opsatRX::set_samp_rate(double samp_rate)
{
  this->samp_rate = samp_rate;
  this->low_pass_filter_0_0->set_taps(gr::filter::firdes::low_pass(1,
                                      this->samp_rate, this->chip_rate, 50e3, filter::firdes::win_type::WIN_HAMMING,
                                      6.76));
}

double opsatRX::get_chip_rate() const
{
  return this->chip_rate;
}

void opsatRX::set_chip_rate(double chip_rate)
{
  this->chip_rate = chip_rate;
  this->low_pass_filter_0_0->set_taps(gr::filter::firdes::low_pass(1,
                                      this->samp_rate, this->chip_rate, 50e3, filter::firdes::win_type::WIN_HAMMING,
                                      6.76));
}


int main(int argc, char *argv[])
{
  std::cout << "Logging to: " << argv[1] << std::endl;
  opsatRX *top_block = new opsatRX(argv[1]);
  top_block->tb->start();
  top_block->tb->wait();

  return 0;
}
