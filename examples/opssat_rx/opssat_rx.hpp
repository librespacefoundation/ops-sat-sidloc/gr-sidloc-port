#ifndef OPSATRX_HPP
#define OPSATRX_HPP
/********************
GNU Radio C++ Flow Graph Header File

Title: Not titled yet
Author: nkaramolegos
GNU Radio version: 3.7.13.5
********************/

/********************
** Create includes
********************/
#include <gnuradio/top_block.h>
#include <gnuradio/analog/agc2_cc.h>
#include <gnuradio/filter/firdes.h>
#include <gnuradio/filter/fir_filter_ccf.h>
#include <sidloc/coarse_freq_estimation.h>
#include <sidloc/ccsds_non_coherent_return.h>
#include <sidloc/opssat_dev_source.h>

using namespace gr;

class opsatRX {

private:

  sidloc::coarse_freq_estimation::sptr sidloc_coarse_freq_estimation_0;
  sidloc::code::code_sptr sidloc_ccsds_non_coherent_return_0;
  gr::filter::fir_filter_ccf::sptr low_pass_filter_0_0;
  analog::agc2_cc::sptr analog_agc2_xx_0;
  sidloc::opssat_dev_source::sptr opssat_dev_source_0;


// Variables:
  double samp_rate = 50e3;
  double chip_rate = 50e3;
  size_t coherent_window;
  size_t dwells;
  double threshold;
  double max_offset = 1.9e3;

public:
  top_block_sptr tb;
  opsatRX(const char *filename);
  ~opsatRX();

  double get_samp_rate() const;
  void set_samp_rate(double samp_rate);
  double get_chip_rate() const;
  void set_chip_rate(double chip_rate);

};


#endif

