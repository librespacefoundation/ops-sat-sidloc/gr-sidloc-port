/********************
GNU Radio C++ Flow Graph Source File

Title: Not titled yet
GNU Radio version: 3.7.13.5
********************/

#include "test_dev2file.hpp"

using namespace gr;


test_grc::test_grc ()  {


    this->tb = gr::make_top_block("OPS-SAT-SIDLOC");

// Blocks:
    this->blocks_file_sink_0 = blocks::file_sink::make(sizeof(int)*1, "/tmp/output_test", false);
    //this->blocks_null_sink_0 = blocks::null_sink::make(sizeof(int));

    //this->analog_sig_source_x_0 = analog::sig_source_c::make(samp_rate, analog::GR_COS_WAVE, 1000, 1, 0);
    this->opssat_dev_source_0 = sidloc::opssat_dev_source::make("/dev/uio0", "/dev/uio2", "/dev/uio1");

// Connections:
    this->tb->hier_block2::connect(this->opssat_dev_source_0, 0, this->blocks_file_sink_0, 0);
    //this->tb->hier_block2::connect(this->blocks_throttle_0, 0, this->blocks_file_sink_0, 0);
}

test_grc::~test_grc () {
}

// Callbacks:
int test_grc::get_samp_rate () const {
    return this->samp_rate;
}

void test_grc::set_samp_rate (int samp_rate) {
    this->samp_rate = samp_rate;
}


int main () {

    test_grc* top_block = new test_grc();
    top_block->tb->start();
    top_block->tb->wait();

    return 0;
}

