#ifndef TEST_GRC_HPP
#define TEST_GRC_HPP
/********************
GNU Radio C++ Flow Graph Header File

Title: Not titled yet
GNU Radio version: 3.7.13.5
********************/

/********************
** Create includes
********************/
#include <gnuradio/top_block.h>
#include <gnuradio/blocks/file_sink.h>
#include <gnuradio/blocks/null_sink.h>
#include <gnuradio/blocks/throttle.h>
//#include <gnuradio/analog/sig_source_c.h>
// #include "opssat_dev_source.h" // test native
#include <sidloc/opssat_dev_source.h>



using namespace gr;



class test_grc {

private:


    blocks::throttle::sptr blocks_throttle_0;
    blocks::file_sink::sptr blocks_file_sink_0;
    blocks::null_sink::sptr blocks_null_sink_0;
    sidloc::opssat_dev_source::sptr opssat_dev_source_0;


// Variables:
    int samp_rate = 32000;

public:
    top_block_sptr tb;
    test_grc();
    ~test_grc();

    int get_samp_rate () const;
    void set_samp_rate(int samp_rate);

};


#endif


