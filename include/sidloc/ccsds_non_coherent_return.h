/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#ifndef INCLUDED_SIDLOC_CCSDS_non_coherent_RETURN_H
#define INCLUDED_SIDLOC_CCSDS_non_coherent_RETURN_H

#include <gnuradio/digital/lfsr.h>
#include <sidloc/api.h>
#include <sidloc/code.h>
#include <mutex>

namespace gr {
namespace sidloc {

/*!
 * \brief Spreading code based on the CCSDS non-coherent return PN code (CCSDS 415.1-B-1)
 *
 */
class SIDLOC_API ccsds_non_coherent_return : public code
{
public:
    static constexpr size_t reg_len = 8;

    static code::code_sptr make_shared(uint32_t seed);

    ccsds_non_coherent_return(uint32_t seed);
    ~ccsds_non_coherent_return();

    size_t length() const;

    bool next();

    void reset();

    std::vector<bool> period();

private:
    const uint32_t m_seed;
    digital::lfsr m_a;
    digital::lfsr m_b;
    size_t m_cnt;
    std::mutex m_mtx;

    void reset_unlocked();
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_CCSDS_NON_COHERENT_RETURN_H */

