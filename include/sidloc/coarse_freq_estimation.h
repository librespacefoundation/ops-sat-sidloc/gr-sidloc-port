/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_COARSE_FREQ_ESTIMATION_H
#define INCLUDED_SIDLOC_COARSE_FREQ_ESTIMATION_H

#include <sidloc/api.h>
#include <gnuradio/sync_block.h>
#include <sidloc/code.h>

namespace gr {
namespace sidloc {

/*!
 * \brief <+description of block+>
 * \ingroup sidloc
 *
 */
class SIDLOC_API coarse_freq_estimation : virtual public gr::sync_block {
public:
  typedef boost::shared_ptr<coarse_freq_estimation> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of sidloc::coarse_freq_estimation.
   *
   * To avoid accidental use of raw pointers, sidloc::coarse_freq_estimation's
   * constructor is in a private implementation
   * class. sidloc::coarse_freq_estimation::make is the public interface for
   * creating new instances.
   */
  static sptr make(double sample_rate,
                   double chip_rate,
                   code::code_sptr c,
                   size_t coherent_window,
                   size_t dwells,
                   double threshold,
                   double max_offset,
                   const char *log_file);

  /**
  * @brief Gets the frequency domain of the locally generated C/A
  *
  * @return std::vector<gr_complex>
  */
  virtual std::vector<gr_complex>
  ca_spectrum() const = 0;
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_COARSE_FREQ_ESTIMATION_H */

