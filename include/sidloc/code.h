/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_CODE_H
#define INCLUDED_SIDLOC_CODE_H

#include <sidloc/api.h>
#include <bitset>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <vector>

namespace gr {
namespace sidloc {

/*!
 * \brief Base class describing a spreading code
 *
 */
class SIDLOC_API code {
public:
  code() {}
  ~code() {}

  // using sptr = std::shared_ptr<code>;
  typedef boost::shared_ptr<code> code_sptr;

  /**
   * @brief Returns the number of bits of the spreading code until it repeats.
   * @note The length refers to the repeat period of the code
   * @return size_t the number of bits until  a repetition of the code occurs
   */
  virtual size_t length() const = 0;

  /**
   * @brief This method retrieves the next bit of the code
   * @return the next bit of the code
   */
  virtual bool next() = 0;

  /**
   * Retrieves a whole period of the spreading sequence for a specific channel.
   * @note This method does not affect the internal state of the sequence in any way
   *
   * @tparam OutputIt output iterator
   * @param it output iterator of a desired container
   */

  // void period(std::vector<bool>::iterator it)
  // {
  //     static_assert("Not implemented");
  // }

  /**
   * @brief Retrieves a whole period of the spreading sequence for a specific channel.
   * @note This method does not affect the internal state of the sequence in any way
   * @return a copy of the code period
   */
  virtual std::vector<bool> period() = 0;

  /**
   * @brief Resets everything to the initial state
   *
   */
  virtual void reset() = 0;
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_CODE_H */
