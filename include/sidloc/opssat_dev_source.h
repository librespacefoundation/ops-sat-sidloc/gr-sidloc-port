/* -*- c++ -*- */
/*
 * Copyright 2022 gr-sidloc author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_SIDLOC_OPSSAT_DEV_SOURCE_H
#define INCLUDED_SIDLOC_OPSSAT_DEV_SOURCE_H

#include <sidloc/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace sidloc {

/*!
 * \brief <+description of block+>
 * \ingroup sidloc
 *
 */
class SIDLOC_API opssat_dev_source : virtual public gr::sync_block {
public:
  typedef boost::shared_ptr<opssat_dev_source> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of sidloc::opssat_dev_source.
   *
   * To avoid accidental use of raw pointers, sidloc::opssat_dev_source's
   * constructor is in a private implementation
   * class. sidloc::opssat_dev_source::make is the public interface for
   * creating new instances.
   */
  static sptr make(const char *ddr_uio_dev_name, const char *dma_uio_dev_name,
                   const char *fifo_arbiter_uio_name);
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_OPSSAT_DEV_SOURCE_H */

