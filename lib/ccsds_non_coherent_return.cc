/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <sidloc/ccsds_non_coherent_return.h>

namespace gr {
namespace sidloc {

/**
 * @brief Creates a shared pointer to a CCSDS non-coherent return code object that is
 * based on Gold sequences
 *
 * @note The CCSDS non-coherent return code has two channels, the I and the Q. However
 * both of them share the same polynomial. Therefore, both channels can be produced
 * with two instances of this class, each one have a separate seed.
 *
 * \ref https://public.ccsds.org/Pubs/415x1b1.pdf section 5.3.3
 *
 * @param seed the seed for the polynomial
 * @return code::code_sptr to a CCSDS non-coherent return code object
 */
code::code_sptr ccsds_non_coherent_return::make_shared(uint32_t seed)
{
  return code::code_sptr(new ccsds_non_coherent_return(seed));
}

ccsds_non_coherent_return::ccsds_non_coherent_return(uint32_t seed)
  : code(),
    m_seed(seed),
    m_a(0x169, seed, reg_len - 1),
    m_b(0x15F, 0x1, reg_len - 1),
    m_cnt(0)
{
}

ccsds_non_coherent_return::~ccsds_non_coherent_return() {}

void ccsds_non_coherent_return::reset()
{
  std::lock_guard<std::mutex> lock(m_mtx);
  reset_unlocked();
}

size_t ccsds_non_coherent_return::length() const
{
  return (1 << reg_len) - 1;
}

bool ccsds_non_coherent_return::next()
{
  std::lock_guard<std::mutex> lock(m_mtx);
  uint8_t b = m_b.next_bit();
  b = m_a.next_bit() ^ b;
  m_cnt++;
  if (m_cnt == length()) {
    reset_unlocked();
  }
  return b;
}


std::vector<bool> sidloc::ccsds_non_coherent_return::period()
{
  std::vector<bool> seq(length(), 0);
  ccsds_non_coherent_return code(m_seed);
  for (size_t i = 0; i < length(); i++) {
    seq[i] = code.next();
  }
  return seq;
}

void sidloc::ccsds_non_coherent_return::reset_unlocked()
{
  m_a.reset();
  m_b.reset();
  m_cnt = 0;
}

} /* namespace sidloc */

} /* namespace gr */


