/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "coarse_freq_estimation_impl.h"
#include <gnuradio/filter/firdes.h>
#include <gnuradio/filter/pfb_arb_resampler.h>
#include <gnuradio/io_signature.h>
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

namespace gr {
namespace sidloc {

coarse_freq_estimation::sptr
coarse_freq_estimation::make(double sample_rate,
                             double chip_rate,
                             code::code_sptr c,
                             size_t coherent_window,
                             size_t dwells,
                             double threshold,
                             double max_offset,
                             const char *log_file)
{
  return gnuradio::get_initial_sptr
         (new coarse_freq_estimation_impl(sample_rate, chip_rate, c, coherent_window,
                                          dwells, threshold, max_offset, log_file));
}

/*
 * The private constructor
 */
coarse_freq_estimation_impl::coarse_freq_estimation_impl(double sample_rate,
    double chip_rate,
    code::code_sptr c,
    size_t coherent_window,
    size_t dwells,
    double threshold,
    double max_offset,
    const char *log_file)
  : gr::sync_block("coarse_freq_estimation",
                   gr::io_signature::make(1, 1, sizeof(gr_complex)),
                   gr::io_signature::make(0,0,0)),
    m_samp_rate(sample_rate),
    m_chip_rate(chip_rate),
    m_code(c),
    m_coherent_window(coherent_window),
    m_dwells(dwells),
    m_threshold(threshold),
    m_fft_len(std::max(m_coherent_window, (size_t)1UL) *
              std::floor(m_samp_rate / (m_chip_rate / m_code->length()))),
    m_max_offset_hz(std::abs(max_offset)),
    m_log_file(log_file),
    m_fft_resolution_hz(m_samp_rate / m_fft_len),
    /* We need */
    /* We will search coarse offset with m_fft_resolution_hz step  */
    m_max_offset_bins(std::ceil(m_max_offset_hz / m_fft_resolution_hz)),
    m_fft(m_fft_len),
    m_ifft(m_fft_len),
    m_seq_resampled(m_fft_len, gr_complex(0, 0)),
    m_seq_resampled_freq(m_fft_len, gr_complex(0.0, 0.0)),
    m_seq_resampled_freq_tmp(m_fft_len, gr_complex(0.0, 0.0)),
    m_circ_corr(m_fft_len, 0.0f),
    m_nocoh_statistic(0),
    m_code_delay(0),
    m_coarse_offset(0),
    m_dwell_count(0)

{
  m_acc_non_coh_data = std::vector<std::vector<float>>
                       (2 * m_max_offset_bins, std::vector<float>(m_fft_len));
  if (m_samp_rate < m_chip_rate) {
    GR_LOG_ERROR(d_logger, "Sampling rate should be larger than the chip rate");
    throw std::invalid_argument("Sampling rate should be larger than the chip rate");
  }

  set_output_multiple(256);
  code_to_samples(sequence_sampling_method::interpolation);
  print_info();
  // Open the file
  std::ofstream file;
  file.open(m_log_file);
  if (file.is_open()) {
    file << "Timestamp, "
         << "Max Pos,"
         << "Max Val,"
         << "Offset" << std::endl;
    file.close();  // close the file
  }
  else {
    std::cerr << "Unable to open file: " << m_log_file << std::endl;
  }

}


/*
 * Our virtual destructor.
 */
coarse_freq_estimation_impl::~coarse_freq_estimation_impl() {}

int
coarse_freq_estimation_impl::work(int noutput_items,
                                  gr_vector_const_void_star &input_items,
                                  gr_vector_void_star &output_items)
{
  auto in = static_cast<const gr_complex *>(input_items[0]);

  /* Copy input to fft input buffer for the circular cross correlation */
  std::copy_n(in, m_fft_len, m_fft.get_inbuf());

  accumulate_non_coherent();
  m_dwell_count++;
  if (m_dwell_count == m_dwells) {
    /* Check for peaks. cross-ambiguity function */
    compute_CAF_statistic();
    if (m_nocoh_statistic > m_threshold) {
      // std::cout <<
      //           boost::format("Doppler coarse offset (Hz)        : %1%") % m_coarse_offset <<
      //           std::endl;
      start_again();
    }
  }

  return m_fft_len;
}

void coarse_freq_estimation_impl::print_info()
{
  std::cout << boost::format("Sampling rate (samples/s)      : %1%") % m_samp_rate
            << std::endl;

  std::cout <<
            boost::format("Chip rate (chips/s)            : %1%") % m_chip_rate <<
            std::endl;
  std::cout <<
            boost::format("Code Length (bits)             : %1%") %
            m_code->length() << std::endl;
  std::cout <<
            boost::format("Maximum Doppler (Hz)           : %1%") % m_max_offset_hz <<
            std::endl;
  std::cout <<
            boost::format("Maximum Doppler (bins)         : %1%") % m_max_offset_bins <<
            std::endl;
  std::cout <<
            boost::format("Resolution Doppler (Hz)        : %1%") % m_fft_resolution_hz <<
            std::endl;
  std::cout <<
            boost::format("FFT Size                       : %1%") % m_fft_len << std::endl;
  std::cout <<
            boost::format("Search Space Duration (s)      : %1%") %
            (1.0 / m_fft_len) << std::endl;
  std::cout <<
            boost::format("Statistic threshold            : %1%") %
            m_threshold << std::endl;
  std::cout <<
            boost::format("Coherent window                : %1%") %
            m_coherent_window << std::endl;
  std::cout <<
            boost::format("Dwell repetitions              : %1%") %
            m_dwells << std::endl;
}

void coarse_freq_estimation_impl::append_to_csv(const char *filename,
    double pos, double max_value,
    double offset)
{

  auto now = std::chrono::system_clock::now();
  auto now_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>
                          (now.time_since_epoch()) % 1000;

  std::time_t now_time = std::chrono::system_clock::to_time_t(now);
  std::tm *now_tm = std::gmtime(&now_time);
  std::ostringstream ss;
  ss << std::put_time(now_tm,
                      "%FT%T.")
     << std::setfill('0') << std::setw(3) << now_milliseconds.count() << "Z";
  // Open the file
  std::ofstream file;
  file.open(filename, std::ios::out | std::ios::app);

  if (file.is_open()) {
    // Write the values to the file
    file << ss.str() << "," << pos << "," << max_value << "," << offset <<
         std::endl;
    file.close();  // close the file
  }
  else {
    std::cerr << "Unable to open file: " << filename << std::endl;
  }
}

std::vector<gr_complex> coarse_freq_estimation_impl::ca_spectrum() const
{
  std::vector<gr_complex> x(m_seq_resampled_freq.size(), gr_complex(0, 0));
  std::copy(m_seq_resampled_freq.cbegin(), m_seq_resampled_freq.cend(),
            x.begin());
  return x;
}

void sidloc::coarse_freq_estimation_impl::code_to_samples(
  sequence_sampling_method m)
{
  switch (m) {
  case sequence_sampling_method::interpolation:
    code_to_samples_interpolation();
    break;
  case sequence_sampling_method::pfb_resampler:
    code_to_samples_pfb_resampler();
    break;
  default:
    throw std::invalid_argument(alias() + ": Invalid sampling method");
  }
}

void coarse_freq_estimation_impl::code_to_samples_interpolation()
{
  const float ts = 1.0 / m_samp_rate;
  const float tc = 1.0 / m_chip_rate;

  const auto period = m_code->period();
//   std::vector<gr_complex> seq_resampled(m_fft_len, gr_complex(0, 0));

  for (size_t i = 0; i < m_fft_len; i++) {
    size_t index = std::ceil(ts * (i + 1) / tc) - 1;
    index = index % period.size();
    assert(index < period.size());
    m_seq_resampled[i] = gr_complex(2.0 * period[index] - 1.0, 0.0);
  }


  std::copy(m_seq_resampled.cbegin(), m_seq_resampled.cend(), m_fft.get_inbuf());
  m_fft.execute();
  volk_32fc_conjugate_32fc(m_seq_resampled_freq.data(), m_fft.get_outbuf(),
                           m_fft_len);
  /* Scale properly the FFT output */
  volk_32f_s32f_normalize(reinterpret_cast<float *>(m_seq_resampled_freq.data()),
                          std::sqrt(m_fft_len),
                          m_fft_len * 2);
}

void coarse_freq_estimation_impl::code_to_samples_pfb_resampler()
{
  /* Resample the spreading code to the target sampling rate */
  std::vector<float> taps = filter::firdes::low_pass(32, 32, 0.4, 0.2);
  filter::kernel::pfb_arb_resampler_ccf resamp(m_samp_rate / m_chip_rate, taps,
      32);
  std::vector<gr_complex> seq;
  auto period = m_code->period();
  for (auto i : period) {
    /* Convert the [0, 1] to [-1, 1] */
    seq.push_back(gr_complex(2.0 * i - 1.0, 0.0));
  }

  /*
   * Due to the arbitrary resampler, allocate a bit more space
   * and after resampling, resize it to the FFT size
   */
  std::vector<gr_complex> seq_resampled(2 * m_fft_len, gr_complex(0, 0));

  /*
   * Resample the entire sequence
   * @note We use only the input size of the original code length.
   * Any left-over samples are zero-padded as the do not affect
   * the spectral content
   */
  size_t input_index = 0;
  size_t output_index = 0;
  while (input_index < seq.size()) {
    int consumed = 0;
    int produced = resamp.filter(seq_resampled.data() + output_index,
                                 seq.data() + input_index,
                                 seq.size() - input_index,
                                 consumed);
    output_index += produced;
    input_index += consumed;
  }

  /* Keep only one FFT window */
  seq_resampled.resize(m_fft_len);
  std::copy(seq_resampled.cbegin(), seq_resampled.cend(), m_fft.get_inbuf());
  m_fft.execute();
  volk_32fc_conjugate_32fc(m_seq_resampled_freq.data(), m_fft.get_outbuf(),
                           m_fft_len);
  /* Scale properly the FFT output */
  volk_32f_s32f_normalize(reinterpret_cast<float *>(m_seq_resampled_freq.data()),
                          std::sqrt(m_fft_len),
                          m_fft_len * 2);
}

void coarse_freq_estimation_impl::accumulate_non_coherent()
{
  /* We have already fill the FFT buffer with samples from input (in)*/
  m_fft.execute();

  // /* Scale properly the FFT output */
  // volk_32f_s32f_normalize(
  //   reinterpret_cast<float *>(m_fft.get_outbuf()), m_fft_len, m_fft_len * 2);


  /* Search on the fc + offset spectrum */
  for (size_t i = 0; i < m_max_offset_bins; i ++) {

    std::rotate_copy(m_seq_resampled_freq.crbegin(),
                     m_seq_resampled_freq.crbegin() + i,
                     m_seq_resampled_freq.crend(),
                     m_seq_resampled_freq_tmp.rbegin());

    volk_32fc_x2_multiply_32fc(m_ifft.get_inbuf(),
                               m_fft.get_outbuf(),
                               m_seq_resampled_freq_tmp.data(),
                               m_fft_len);
    m_ifft.execute();
    volk_32fc_magnitude_squared_32f(
      m_circ_corr.data(), m_ifft.get_outbuf(), m_fft_len);

    // accumulate for dwell values
    volk_32f_x2_add_32f(m_acc_non_coh_data[i].data(),
                        m_acc_non_coh_data[i].data(), m_circ_corr.data(),
                        m_fft_len);
  }

  /* Search on the fc - offset spectrum */
  for (size_t i = 1; i <= m_max_offset_bins; i ++) {

    std::rotate_copy(m_seq_resampled_freq.cbegin(),
                     m_seq_resampled_freq.cbegin() + i,
                     m_seq_resampled_freq.cend(),
                     m_seq_resampled_freq_tmp.begin());


    volk_32fc_x2_multiply_32fc(m_ifft.get_inbuf(),
                               m_fft.get_outbuf(),
                               m_seq_resampled_freq_tmp.data(),
                               m_fft_len);
    m_ifft.execute();
    volk_32fc_magnitude_squared_32f(
      m_circ_corr.data(), m_ifft.get_outbuf(), m_fft_len);

    // accumulate for dwell values
    volk_32f_x2_add_32f(m_acc_non_coh_data[m_max_offset_bins + i - 1].data(),
                        m_acc_non_coh_data[m_max_offset_bins + i - 1].data(), m_circ_corr.data(),
                        m_fft_len);
  }
  /* Note that we arrange the elemements in a way that fftshift is perfomed */
}

void coarse_freq_estimation_impl::compute_CAF_statistic()
{
  float firstPeak = 0.0;
  size_t index_doppler = 0;
  uint32_t tmp_intex_t = 0;
  uint32_t index_time = 0;

  // Look for correlation peaks in the results ==============================
  // Find the highest peak and compare it to the second highest peak
  // The second peak is chosen not closer than 1 chip to the highest peak
  // --- Find the correlation peak and the carrier frequency --------------
  for (size_t i = 0; i < 2 * m_max_offset_bins; i ++) {
    volk_32f_index_max_32u(&tmp_intex_t, m_acc_non_coh_data[i].data(), m_fft_len);
    if (m_acc_non_coh_data[i][tmp_intex_t] > firstPeak) {
      firstPeak = m_acc_non_coh_data[i][tmp_intex_t];
      index_doppler = i;
      index_time = tmp_intex_t;
    }
  }

  // -- - Find 1 chip wide code phase exclude range around the peak
  uint32_t samplesPerChip = ceil(static_cast<float>(m_samp_rate) /
                                 static_cast<float>(m_chip_rate));
  int excludeRangeIndex1 = index_time - samplesPerChip;
  int excludeRangeIndex2 = index_time + samplesPerChip;

  // -- - Correct code phase exclude range if the range includes array boundaries
  if (excludeRangeIndex1 < 0) {
    excludeRangeIndex1 = m_fft_len + excludeRangeIndex1;
  }
  else if (excludeRangeIndex2 >= static_cast<int>(m_fft_len)) {
    excludeRangeIndex2 = excludeRangeIndex2 - m_fft_len;
  }

  int idx = excludeRangeIndex1;
  do {
    m_acc_non_coh_data[index_doppler][idx] = 0.0;
    idx++;
    if (idx ==  static_cast<int>(m_fft_len)) {
      idx = 0;
    }
  }
  while (idx != excludeRangeIndex2);

  // --- Find the second highest correlation peak in the same freq. bin ---
  volk_32f_index_max_32u(&tmp_intex_t, m_acc_non_coh_data[index_doppler].data(),
                         m_fft_len);
  float secondPeak = m_acc_non_coh_data[index_doppler][tmp_intex_t];

  // 5- Compute the test statistics and compare to the threshold
  m_nocoh_statistic = firstPeak / secondPeak;

  m_code_delay = index_time;

  /* fftshift has NOT perfomed */

  // std::cout <<  boost::format("Index       : %1%") % index_doppler << std::endl;

  if (index_doppler >= m_max_offset_bins) {
    /* Negative frequencies, you have to shift it m_max_offset_bins to find the correct frequency.
    Think like fftshift  */
    m_coarse_offset = double(index_doppler - m_max_offset_bins + 1) *
                      (-m_fft_resolution_hz);
  }
  else if (index_doppler < m_max_offset_bins) {
    /* Positive frequencies, it ok */
    m_coarse_offset = index_doppler * m_fft_resolution_hz;
  }

  append_to_csv(m_log_file, index_doppler, firstPeak, m_coarse_offset);

}

void coarse_freq_estimation_impl::start_again()
{
  m_dwell_count = 0;
  for (auto &i : m_acc_non_coh_data) {
    std::fill(i.begin(), i.end(), 0);
  }
}

} /* namespace sidloc */
} /* namespace gr */

