/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#ifndef INCLUDED_SIDLOC_COARSE_FREQ_ESTIMATION_IMPL_H
#define INCLUDED_SIDLOC_COARSE_FREQ_ESTIMATION_IMPL_H

#include <gnuradio/fft/fft.h>
// #include <gnuradio/fft/fft_shift.h>
#include <sidloc/coarse_freq_estimation.h>
#include <volk/volk.h>


namespace gr {
namespace sidloc {
class coarse_freq_estimation_impl : public coarse_freq_estimation {
private:
  // Nothing to declare in this block.

public:
  enum class sequence_sampling_method { interpolation, pfb_resampler };
  coarse_freq_estimation_impl(double sample_rate,
                              double chip_rate,
                              code::code_sptr c,
                              size_t coherent_window,
                              size_t dwells,
                              double threshold,
                              double max_offset,
                              const char *log_file);
  ~coarse_freq_estimation_impl();

  // Where all the action really happens
  int work(int noutput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

  std::vector<gr_complex> ca_spectrum() const;

private:
  const double m_samp_rate;
  const double m_chip_rate;
  code::code_sptr m_code;
  const size_t m_coherent_window;
  const size_t m_dwells;
  const double m_threshold;
  const size_t m_fft_len;
  const double m_max_offset_hz;
  const char *m_log_file;
  const double m_fft_resolution_hz;
  const size_t m_max_offset_bins;
  fft::fft_complex m_fft;
  fft::fft_complex m_ifft;
  std::vector<gr_complex> m_seq_resampled;
  std::vector<gr_complex> m_seq_resampled_freq;
  std::vector<gr_complex> m_seq_resampled_freq_tmp;
  std::vector<float> m_circ_corr;
  double m_nocoh_statistic;
  size_t m_code_delay;
  double m_coarse_offset;
  size_t m_dwell_count;
  std::vector<std::vector<float>> m_acc_non_coh_data;


  void print_info();

  void append_to_csv(const char *filename, double pos, double max_value,
                     double offset);

  void code_to_samples(sequence_sampling_method m);

  void code_to_samples_interpolation();
  void code_to_samples_pfb_resampler();

  void accumulate_non_coherent();
  void compute_CAF_statistic();
  void estimate_fine_doppler();
  void start_again();
};
} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_COARSE_FREQ_ESTIMATION_IMPL_H */

