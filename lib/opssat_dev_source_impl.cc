/* -*- c++ -*- */
/*
 * Copyright 2022 gr-sidloc author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "opssat_dev_source_impl.h"


namespace gr {
namespace sidloc {

opssat_dev_source::sptr
opssat_dev_source::make(const char *ddr_uio_dev_name,
                        const char *dma_uio_dev_name, const char *fifo_arbiter_uio_name)
{
  return gnuradio::get_initial_sptr
         (new opssat_dev_source_impl(ddr_uio_dev_name, dma_uio_dev_name, fifo_arbiter_uio_name));
}

/*
 * The private constructor
 */
opssat_dev_source_impl::opssat_dev_source_impl(const char *ddr_uio_dev_name,
    const char *dma_uio_dev_name, const char *fifo_arbiter_uio_name)
  : gr::sync_block("opssat_dev_source",
                   gr::io_signature::make(0, 0, 0),
                   gr::io_signature::make(1, 1, sizeof(gr_complex))),
    m_produced_bytes(DESC_PER_CHAIN*LEN_PER_DESCRIPTOR),
    m_produced_samples(m_produced_bytes/sizeof(uint32_t))
{
  m_opssat_dev = new opssat_sidloc(ddr_uio_dev_name, dma_uio_dev_name);
  m_opssat_dev->activate_stream();
  m_in_buffer = (int16_t *) volk_malloc(m_produced_samples *2* sizeof(int16_t),
                                        volk_get_alignment());
  set_output_multiple(m_produced_samples);
  if (!m_in_buffer) {
    throw std::runtime_error("Could not allocate memory");
  }
}

/*
 * Our virtual destructor.
 */
opssat_dev_source_impl::~opssat_dev_source_impl()
{
  delete m_opssat_dev;
  volk_free(m_in_buffer);
}

int
opssat_dev_source_impl::work(int noutput_items,
                             gr_vector_const_void_star &input_items,
                             gr_vector_void_star &output_items)
{
  gr_complex *o = (gr_complex *)output_items[0];
  int ret;
  ret = m_opssat_dev->read_stream(reinterpret_cast<uint32_t *>(m_in_buffer), m_produced_bytes);

  if(ret < 0 ) {
    m_opssat_dev->reset_device();
    return m_produced_samples;
  }

  // /* Convert to gr_complex */
  volk_16i_s32f_convert_32f(reinterpret_cast<float *>(o), m_in_buffer, (1 << 15) - 1,
                            2*m_produced_samples);


  // Tell runtime system how many output items we produced.
  return m_produced_samples;
}

} /* namespace sidloc */
} /* namespace gr */

