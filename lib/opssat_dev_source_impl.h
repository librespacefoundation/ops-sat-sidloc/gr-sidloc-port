/* -*- c++ -*- */
/*
 * Copyright 2022 gr-sidloc author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_SIDLOC_OPSSAT_DEV_SOURCE_IMPL_H
#define INCLUDED_SIDLOC_OPSSAT_DEV_SOURCE_IMPL_H

#include <sidloc/opssat_dev_source.h>
#include <volk/volk.h>
#include "device_api/opssat_sidloc.hpp"

namespace gr {
namespace sidloc {

class opssat_dev_source_impl : public opssat_dev_source {

public:
  opssat_dev_source_impl(const char *ddr_uio_dev_name,
                         const char *dma_uio_dev_name, const char *fifo_arbiter_uio_name);
  ~opssat_dev_source_impl();

  // Where all the action really happens
  int work(int noutput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
private:
  uint32_t m_produced_bytes;
  uint32_t m_produced_samples;
  opssat_sidloc *m_opssat_dev;
  int16_t *m_in_buffer;
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_OPSSAT_DEV_SOURCE_IMPL_H */

