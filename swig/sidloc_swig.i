/* -*- c++ -*- */

#define SIDLOC_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "sidloc_swig_doc.i"


%nodefaultctor gr::sidloc::code;
%template(code_sptr) boost::shared_ptr<gr::sidloc::code>;


%{
#include "sidloc/code.h"
#include "sidloc/ccsds_non_coherent_return.h"
#include "sidloc/coarse_freq_estimation.h"
#include "sidloc/opssat_dev_source.h"
%}


%include "sidloc/code.h"
%include "sidloc/ccsds_non_coherent_return.h"
%include "sidloc/coarse_freq_estimation.h"
GR_SWIG_BLOCK_MAGIC2(sidloc, coarse_freq_estimation);
%include "sidloc/opssat_dev_source.h"
GR_SWIG_BLOCK_MAGIC2(sidloc, opssat_dev_source);
